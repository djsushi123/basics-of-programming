package vetclinic;

public class Service {

    private int code;
    private String name;
    private int cost;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        if (code <= 0) {
            System.out.println("Wrong code!");
            return;
        }
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null) {
            System.out.println("Uninitialized name!");
            return;
        }
        this.name = name;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        if (cost <= 100) {
            System.out.println("Cost should be > 100.");
        } else {
            this.cost = cost;
        }
    }

    public void addService(int code, String name, int cost) {
        setCode(code);
        setName(name);
        setCost(cost);

        System.out.println("Code: " + code);
        System.out.println("Name: " + name);
        System.out.println("Cost: " + cost);
    }

    public static void main(String[] args) {

    }
}
