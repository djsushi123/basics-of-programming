package finalassignment2021;

import java.util.ArrayList;

public class FishMan {

    static int poison = 0;
    static int eatable = 0;
    static ArrayList<Fish> fishes = new ArrayList<>();

    public static void youCaught(Fish fish) {
        fishes.add(fish);
    }

    public static void myBasket() {
        System.out.println("Fishes number: " + fishes.size());
        for (int i = 0; i < fishes.size(); i++) {
            Fish myFish = fishes.get(i);
            System.out.println(myFish.fishInfo());
            System.out.printf("%d: L: %.2fcm, W: %.2fkg\n", i, myFish.getFishLength(), myFish.getFishWeight());
            if (myFish.poison) poison++; else eatable++;
        }
        System.out.println("Eatable: " + eatable);
        System.out.println("Poison: " + poison);
    }

    public static void fishDayInfo() {
        if (poison * 3 < eatable)
            System.out.println("Great day!");
        else if (poison * 2 < eatable)
            System.out.println("Good day!");
        else if (poison < eatable)
            System.out.println("Normal day!");
        else if (poison > eatable * 3)
            System.out.println("Horrible day!");
        else
            System.out.println("Bad day!");
    }

}
