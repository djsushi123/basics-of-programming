package finalassignment2021;

public class Fish {

    private double fishLength;
    private double fishWeight;
    boolean poison;

    public Fish(double fishLength, double fishWeight) {
        this.fishLength = fishLength;
        this.fishWeight = fishWeight;
    }

    public String fishInfo() {
        return "Is poison? " + poison;
    }

    public double getFishLength() {
        return fishLength;
    }

    public void setFishLength(double fishLength) {
        this.fishLength = fishLength;
    }

    public double getFishWeight() {
        return fishWeight;
    }

    public void setFishWeight(double fishWeight) {
        this.fishWeight = fishWeight;
    }
}
