package coderunner2;

public class Dimmer extends LightSwitch {

    int brightness;

    public void setBrightness(int brightness) {
        this.brightness = brightness;
        lightsOn = brightness > 0;
    }

    @Override
    void printInfo() {
        if (lightsOn)
            System.out.println("Brightness is " + brightness + ".");
        else
            System.out.println("Lights are off.");
    }
}
