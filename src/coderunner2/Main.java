package coderunner2;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        System.out.println("Width?");
        int width = s.nextInt();
        s.nextLine();

        System.out.println("Length?");
        int length = s.nextInt();
        s.nextLine();

        System.out.println("Area is " + Utils.areaCalc(width, length));
    }

}
