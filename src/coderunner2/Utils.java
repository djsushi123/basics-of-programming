package coderunner2;

class Utils {

    static void printTitle() {
        System.out.println("*******");
        System.out.println("* OOP *");
        System.out.println("*******");
    }

    static int areaCalc(int width, int length) {
        return width * length;
    }

}
