package pacman;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        Animal a = new Animal();
        Feline f = new Feline();
        Tiger t = new Tiger();
        Puppy p = new Puppy();

        ArrList<Animal> list = new ArrList<>();
        list.add(f);
        list.add(p);
        list.add(t);
        list.add(a);

    }
}

class ArrList<T> {

    private Object[] backing = new Object[5];
    int currentSize = 0;

    public void set(int index, T item) {
        if (index < currentSize)
            backing[index] = item;
        throw new ArrayIndexOutOfBoundsException("U stoopid. Index too chunky.");
    }

    public void add(T item) {
        if (currentSize == backing.length) {
            // copy everything from backing
            Object[] temp = new Object[currentSize];
            System.arraycopy(backing, 0, temp, 0, currentSize);
            backing = new Object[currentSize * 2];
            currentSize *= 2;
            // copy everything from temporary storage to backing
            if (currentSize >= 0) System.arraycopy(temp, 0, backing, 0, currentSize);
        }
    }

    public T get(int index) {
        if (index < currentSize)
            return (T) backing[index];
        throw new ArrayIndexOutOfBoundsException("U stoopid. Index too big big.");
    }

}

class Animal {
    void breathe() {
        System.out.println("I am breathing.");
    }
}
class Puppy extends Animal {}
class Feline extends Animal {}
class Tiger extends Feline {}
