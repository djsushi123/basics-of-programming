package staticmethodsandoverloading;

class Engine {

    double mass, power;
    public static final double mu = 0.3; // SI
    public static final double g = 9.8; // m/s^2

    public Engine(double mass, double power) {
        this.mass = mass;
        this.power = power;
    }

    public double getMass() {
        return mass;
    }

    public double getForce(double velocity) {
        double f1 = power / velocity;
        double f2 = mass * g * mu;
        return Math.min(f1, f2);
    }

    static double getForce(double power, double velocity, double mass) {
        double f1 = power / velocity;
        double f2 = mass * g * mu;
        return Math.min(f1, f2);
    }
}
