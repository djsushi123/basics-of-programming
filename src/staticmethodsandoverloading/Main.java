package staticmethodsandoverloading;

public class Main {

    public static void main(String[] args) {
        Engine engine = new Engine(100, 100);
        System.out.println(engine.getForce(50));
        System.out.println(Engine.getForce(100, 50, 100));

        Car anotherCar = new Car(500, "Peugeot");
        System.out.println("The current car count is " + Car.carCount);
        testMethod();
        System.out.println("The current car count is " + Car.carCount);

    }

    static void testMethod() {
        Car volvoCar = new Car(100, "Volvo");
        Car citroenCar = new Car(500, "Citroen");
        System.out.println("The current car count is " + Car.carCount);

        volvoCar.setMass(60_000);
        System.out.println(volvoCar.avgMass);
    }

}