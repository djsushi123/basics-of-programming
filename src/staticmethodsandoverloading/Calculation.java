package staticmethodsandoverloading;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class Calculation {

    static double calcArea(double width, double length) {
        return width * length;
    }

    static double calcArea(double radius) {
        // we're casting here! Check out "Java casting" online to find out more.
        return Math.PI * radius * radius;
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        DecimalFormat df = new DecimalFormat("#.##");
        String choice = null;

        do {
            System.out.print("Calculate the area of a rectangle or a circle? (r/c) ");
            String input = s.nextLine().toLowerCase(Locale.ROOT);
            if (input.equals("r") || input.equals("c"))
                choice = input;
        } while (choice == null);

        Exception e;

        do {
            e = null;
            try {
                if (choice.equals("r")) {
                    System.out.print("Width? ");
                    double width = s.nextDouble();
                    s.nextLine();
                    System.out.print("Length? ");
                    double length = s.nextDouble();
                    s.nextLine();
                    System.out.println("Area is " +  df.format(calcArea(width, length)) + ".");
                } else {
                    System.out.print("Radius? ");
                    double radius = s.nextDouble();
                    s.nextLine();
                    System.out.println("Area is " + df.format(calcArea(radius)) + ".");
                }
            } catch (Exception caught) {
                e = caught;
                System.out.println("You didn't enter the number correctly! Exiting and starting again...");
                System.out.println();
                s.nextLine();
            }
        } while (e != null);
    }

}
