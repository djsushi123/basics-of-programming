package loops.advanced;

import java.util.Scanner;

public class AdvLoops001 {

    public static void main(String[] args) {

        // we create out scanner object
        Scanner in = new Scanner(System.in);

        // ask for the first number
        System.out.println("First number?");
        int num1 = in.nextInt();

        // ask for the second one
        System.out.println("Last number?");
        int num2 = in.nextInt();

        // we go through all the numbers between num1 and num2 including those numbers and print every one of them
        for (int i = num1; i <= num2; i++) {
            System.out.println(i);
        }

    }

}
