package bonus.exercise1;

public class ElectricCar extends Car {

    private String name, brand;

    ElectricCar(String name, String brand) {

    }

    @Override
    public void setRange(double range) {
        super.setRange(range * 5);
    }

    @Override
    void range() {
        System.out.println("******Electric car******");
        super.range();
    }

    @Override
    double tank(double input) {
        System.out.println("You cannot tank gas into the electric car!");
        return input*0;
    }
}