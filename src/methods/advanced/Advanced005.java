package methods.advanced;

public class Advanced005 {

    public void printTitle(boolean printStars) {

        System.out.println("The Title");

        // we don't need to write "if (printStars == true)" because the if statement doesn't require us to
        // compare some things. It just needs the expression in the parentheses to be true. So, if [printStars] is
        // true, then the if statement gets executed.
        if (printStars)
            System.out.println("*********");

    }

}
