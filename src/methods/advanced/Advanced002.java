package methods.advanced;

public class Advanced002 {

    // the assignment tells you to make a method called "printAphorism"
    // but the method should really be called "aphorismPrinter"
    public void aphorismPrinter(int index) {

        // we first define the aphorisms in an array
        String[] aphorisms = {
                "Actions speak louder than words.",
                "A barking dog never bites.",
                "A penny saved is a penny earned.",
                "All things come to those who wait."
        };

        // then, we print the aphorism at the index of [index] that the user has input
        System.out.println(aphorisms[index]);
    }

}
