package classes.car003;

class Car {

    private String brand;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    private String model;
    private int amountOfFuel;

    public Car() {
        brand = "";
        model = "";
        amountOfFuel = 0;

        printData();
    }

    public Car(String brand, String model, int amountOfFuel) {
        this.brand = brand;
        this.model = model;
        this.amountOfFuel = amountOfFuel;

        printData();
    }

    public void brake() {
        System.out.println("Car is breaking");
    }

    public void accelerate() {
        if (amountOfFuel > 0) System.out.println("Car is accelerating");
        amountOfFuel--;
    }

    private void printData() {
        System.out.println("Brand: " + brand);
        System.out.println("Model: " + model);
        System.out.println("Fuel: " + amountOfFuel);
    }
}
