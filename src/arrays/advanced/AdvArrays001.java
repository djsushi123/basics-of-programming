package arrays.advanced;

import java.util.Scanner;

public class AdvArrays001 {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        // here, we specify that this array will have the length 3
        double[] javelinThrows = new double[3];

        // get the three throws
        for (int i = 0; i < 3; i++) {
            System.out.println("Throw length");
            javelinThrows[i] = in.nextDouble();
        }

        // loop from 1 to the number of throws inside javelinThrows. Print every value.
        for (int i = 1; i <= javelinThrows.length; i++) {
            System.out.println("Throw " + i + ": " + javelinThrows[i - 1]);
        }
    }

}
