package arrays.basics;

import java.util.Scanner;

public class Arrays002 {

    public static void main(String[] args) {

        // We define out Scanner. This is an object which helps us get input from the user.
        Scanner in = new Scanner(System.in);

        // We can initialize this array like this:
        String[] array = {
                "Actions speak louder than words.",
                "A barking dog never bites.",
                "A penny saved is a penny earned.",
                "All things come to those who wait."
        };
        // Notice that every phrase is on a different line. I have done this to make it easier to read.

        // We ask the user to pick a number.
        System.out.println("Pick number from 1-4.");

        // We get the input from the user
        int input = in.nextInt();

        // We write the correct phrase. Notice the "input - 1". It is there because the indices in Java start at 0.
        // Thus, if the user writes "1", we want to actually show index 1 - 0 = 0 because 0 is the first index in Java.
        System.out.println(array[input - 1]);

    }

}
