# Info about the test on Monday 27.09.2021

There are some rules you should follow if you want your code to 
run correctly in CodeRunner. Here are they:

### Does the question have anything to do with methods?

If yes, then copy over ONLY the method from your code.

If this is your code:
```java
package methods.basics;

public class Basics002 {

    public static void main(String[] args) {
        printNumbers();
    }
    
    public static void printNumbers() {
        for (int i = 0; i <= 9; i++) {
            System.out.println(i);
        }
    }

}
```

Then only copy this part:

```java
public void printNumbers() {
    for (int i = 0; i <= 9; i++) {
        System.out.println(i);
    }
}
```

If the question has nothing to do with methods, then REMOVE the 
package! Removing the package is very important for the code to 
run properly. So...

If this is your code:
```java
package arrays.basics;

public class Arrays006 {

    public static void main(String[] args) {

        int[] array = {16, 18, 5, 3, 10};
        
        int minimum = array[0];

        for (int i : array) {
            if (i < minimum)
                minimum = i;
        }

        System.out.println(minimum);
    }
}
```

Then you copy over everything **except** the package:
```java
public class Arrays006 {

    public static void main(String[] args) {

        int[] array = {16, 18, 5, 3, 10};
        
        int minimum = array[0];

        for (int i : array) {
            if (i < minimum)
                minimum = i;
        }

        System.out.println(minimum);
    }
}
```