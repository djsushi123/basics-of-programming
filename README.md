# Basics of Programming

This is a repository that holds (almost) all solutions to the Basics of Programming course.

## Disclaimer

This repo is not supposed to be a source of material you can just copy and submit as your own work! Doing so is called
plagiarism, and it's a serious crime. I am not responsible for any problems you'll cause yourself by copying and
submitting the code as your own.

## How to run it yourself

Every program is in the src/ directory. Just click on it and find the programs. They should be categorized by assignment
names. I'm going to give you an example. Let's start with my Arrays001 program:

```java
package arrays;

public class Arrays001 {

	public static void main(String[] args) {

		// here, we create our array and initialize it with the correct values
		String[] array = new String[]{"Green", "Blue", "Yellow"};

		// we use index "1" and not "2" because in Java, arrays start counting at 0. That means that when we want
		// the second element, we count 0, 1... Thus, index 1 is actually the second element inside an array.
		System.out.println(array[1]);

		// This is called an enhanced loop. We can read this statement as "for every String 's' inside array,
		// print the String"
		for (String s : array) {
			System.out.println(s);
		}

	}

}
```

This program has `package arrays;` at the beginning, which is the name of the package the program is currently in. This
is specific to my program, and you should either remove it or change it into your own package name.

<h3>That's it!</h3>

### Happy coding! :)
